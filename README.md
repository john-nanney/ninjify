
# Ninjify

Creates a ninja build file from a build description in ron, json, yaml, or toml.

Optionally can create a generic makefile calling ninja for editors or IDEs that support makefiles.

Usage:

```
ninjify my_nifty_project.json
```

See the example files for details.


### Rationale

I do use CMake for building c/c++ projects, but I often don't need the
configuration features, and it's nice not having to install CMake on the target
if it's an embedded platform.

Also this is a fun little project to learn a little about the rust programming language.
