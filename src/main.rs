
use std::process::Command;
use nanoserde::{DeJson,DeRon};

use std::fs::File;
use std::io::prelude::*;

#[derive(Debug, DeJson, DeRon, Clone)]
struct Target {
    name: String,
    #[nserde(default)]
    cflags: Vec<String>,
    #[nserde(default)]
    ldflags: Vec<String>,
    #[nserde(default)]
    libs: Vec<String>,
    #[nserde(default)]
    sources: Vec<String>,
    #[nserde(default)]
    library: Option<String>,
}

/// Structure containing the build information.
///
/// compiler will default to "clang"
/// cflags and ldflags will default to empty
///
/// There must be at least one target.
#[derive(Debug, DeJson, DeRon, Clone)]
struct Buildinfo {
    #[nserde(default)]
    makefile: bool,
    #[nserde(default)]
    compiler: String,
    #[nserde(default)]
    cflags: Vec<String>,
    #[nserde(default)]
    ldflags: Vec<String>,
    #[nserde(default)]
    libs: Vec<String>,
    targets: Vec<Target>,
}

impl Buildinfo {

    pub fn to_ninja_string(&self) -> String {

        let mut ninja_string = "\nninja_required_version = 1.6\nroot = .\nbuilddir = .target\n".to_string();

        if self.compiler.is_empty() {
            ninja_string += "\ncompiler = clang\n\nlinker = clang\n";
        } else {
            ninja_string += format!("\ncompiler = {}\n", self.compiler).as_str();
            ninja_string += format!("\nlinker = {}\n", self.compiler).as_str();
        }

        if !self.cflags.is_empty() {
            ninja_string += format!("\ncflags = {}\n", self.cflags.join(" ")).as_str();
        }

        if !self.ldflags.is_empty() {
            ninja_string += format!("\nldflags = {}\n", self.ldflags.join(" ")).as_str();
        }

        for t in self.targets.iter() {

            if !t.cflags.is_empty() {
                ninja_string += format!("\n{}_cflags = {}\n", t.name, t.cflags.join(" ")).as_str();
            }

            if !t.ldflags.is_empty() {
                ninja_string += format!("\n{}_ldflags = {}\n", t.name, t.ldflags.join(" ")).as_str();
            }

        }

        // The build lines can only contain dependency stuff, so we have to make a compile and link
        // command for each target in order to have target specific flags.

        for t in self.targets.iter() {

            ninja_string += format!("\nrule compile_{}\n", t.name).as_str();
            ninja_string += format!("  command = $compiler -MMD -MT $out -MF $out.d $cflags ${}_cflags -c $in -o $out\n", t.name).as_str();
            ninja_string += "  description = COMPILE $out\n";
            ninja_string += "  depfile = $out.d\n";
            ninja_string += "  deps = gcc\n";

            ninja_string += format!("\nrule link_{}\n", t.name).as_str();
            ninja_string += format!("  command = $compiler $ldflags ${}_ldflags -o $out $in $libs\n", t.name).as_str();
            ninja_string += "  description = LINK $out\n";

        }

        for t in self.targets.iter() {

            for s in t.sources.iter() {
                ninja_string += format!("\nbuild $builddir/{0}_{1}.o: compile_{0} $root/{1}\n", t.name, s).as_str();
            }

        }

        for t in self.targets.iter() {
            let all_objects = t.sources.join(format!(".o $builddir/{}_", t.name).as_str());
            ninja_string += format!("\nbuild {0}: link_{0} $builddir/{0}_{1}.o", t.name, all_objects).as_str();

            if !self.libs.is_empty() || !t.libs.is_empty() {

                ninja_string += " |\n";

                ninja_string += "    libs = ";

                if !self.libs.is_empty() {
                    ninja_string += format!(" -l{} ", self.libs.join(" -l")).as_str().trim();
                }

                if !t.libs.is_empty() {
                    ninja_string += format!(" -l{} ", t.libs.join(" -l")).as_str().trim();
                }

            } else {
                ninja_string += "\n";
            }

            ninja_string += "\n";
        }

        let all_targets: String =
            self.targets.iter().map(|t| {
                t.name.clone()
            }).collect::<Vec<String>>().join(" ");

        ninja_string += format!("\nbuild all: phony {}\n", all_targets).as_str();

        ninja_string

    } // to_ninja_string()

    pub fn to_cmake_string(&self) -> String {

        let mut cmake_string = "\ncmake_minimum_required(VERSION 3.0)\n".to_string();

        cmake_string += "\nset(CMAKE_EXPORT_COMPILE_COMMANDS On)\n";

        // Ignore the compiler, that should be set at the CMake level anyway

        if !self.cflags.is_empty() {
            cmake_string += "\nadd_compile_options(\n";
            for flag in &self.cflags {
                cmake_string += "    ";
                cmake_string += flag;
                cmake_string += "\n";
            }
            cmake_string += "    )\n";
        }

        if !self.ldflags.is_empty() {
            cmake_string += "\nlink_libraries(\n";
            for flag in &self.ldflags {
                cmake_string += "    ";
                cmake_string += flag;
                cmake_string += "\n";
            }
            cmake_string += "    )\n";
        }

        for t in self.targets.iter() {

            if !t.cflags.is_empty() {
                cmake_string += format!("\ntarget_compile_options({} PRIVATE\n", t.name).as_str();
                for flag in &t.cflags {
                    cmake_string += "    ";
                    cmake_string += flag;
                    cmake_string += "\n";
                }
                cmake_string += "    )\n";
            }

            if let Some(library) = &t.library {
                cmake_string += "\nadd_library(";
                cmake_string += t.name.as_str();
                cmake_string +=
                if "static" == library {
                    "\n    STATIC"
                } else {
                    "\n    SHARED"
                };
            } else {
                cmake_string += "\nadd_executable(";
                cmake_string += t.name.as_str();
            }
            cmake_string += "\n";
            for source in &t.sources {
                    cmake_string += "    ";
                    cmake_string += source;
                    cmake_string += "\n";
            }
            cmake_string += "    )\n";

            if !t.ldflags.is_empty() {
                cmake_string += "\ntarget_link_libraries(";
                cmake_string += t.name.as_str();
                cmake_string += "\n";
                for flag in &t.ldflags {
                    cmake_string += "    ";
                    cmake_string += flag;
                    cmake_string += "\n";
                }
                cmake_string += "    )\n";
            }

        }

        cmake_string
    }

    pub fn write_build_file(&self) -> std::result::Result<Self,String> {

        let ninja_string = self.to_ninja_string();

        { // scope for file
            let mut file = File::create("build.ninja").unwrap();
            file.write_all(ninja_string.as_bytes()).unwrap();
        }

        if self.makefile {
            let mut mkfile = File::create("Makefile").unwrap();
            mkfile.write_all(b"\n.PHONY: all\n\n").unwrap();
            mkfile.write_all(b"\nall:\n").unwrap();
            mkfile.write_all(b"	@ninja").unwrap();

            self.targets.iter().for_each(|t| mkfile.write_all(format!("\n.PHONY: {0}\n\n\n{0}:\n	@ninja {0}\n", t.name).as_bytes()).unwrap());

            mkfile.write_all(b"\n.PHONY: clean\n\nclean:\n	@ninja -t clean\n").unwrap();
        }

        Ok(self.clone())

    } // write_build_file()

    pub fn write_cmake_file(&self) -> std::result::Result<Self,String> {
        let cmake_string = self.to_cmake_string();

        { // scope for file
            let mut file = File::create("CMakeLists.txt").unwrap();
            file.write_all(cmake_string.as_bytes()).unwrap();
        }
        Ok(self.clone())
    } // write_cmake_file()
}

fn load_from_file(source_name: &str) -> std::result::Result<Buildinfo,String> {

    let mut file = File::open(source_name).unwrap();
    let mut contents = String::new();
    file.read_to_string(&mut contents).unwrap();

    // Convert to read-only
    let contents = contents;

    let result: Result<Buildinfo,_> = DeJson::deserialize_json(contents.as_str());

    if let Ok(binfo) = result {
        Ok(binfo)
    } else {
        let result: Result<Buildinfo,_> = DeRon::deserialize_ron(contents.as_str());
        if let Ok(binfo) = result {
            Ok(binfo)
        } else {
            Err(format!("Failed to read {} -- check the syntax", source_name))
        }
    }

} // load_from_file()

fn load_from_file_and_write_ninja(source_name: &str) -> std::result::Result<(),String> {

    let result = load_from_file(source_name);

    if let Ok(binfo) = result {
        if binfo.write_build_file().is_ok() {
            return Ok(());
        }
    }

    Err(format!("Failed to read {} -- check the syntax", source_name))

} // load_from_file_and_write_ninja()

fn load_from_file_and_write_cmake(source_name: &str) -> std::result::Result<(),String> {

    let result = load_from_file(source_name);

    if let Ok(binfo) = result {
        if binfo.write_cmake_file().is_ok() {
            return Ok(());
        }
    }

    Err(format!("Failed to read {} -- check the syntax", source_name))

} // load_from_file_and_write_cmake()

fn write_compile_commands() {

    let output = Command::new("ninja")
        .arg("-t")
        .arg("compdb")
        .output()
        .expect("Failed to execute ninja to get compile commands");

    let mut file = File::create("compile_commands.json").unwrap();
    file.write_all(output.stdout.as_slice()).unwrap();
}

fn run_build() {
    let _ = Command::new("ninja")
        .output()
        .expect("Failed to execute ninja to build");
}

fn run_samurai() {
    let _ = Command::new("samu")
        .arg("-j0")
        .output()
        .expect("Failed to execute samu to build");
}

struct Args {
    help: bool,
    version: bool,
    commands: bool,
    cmake: bool,
    run_build: bool,
    run_samurai: bool,
    file: String,
}

fn main() {

    let mut args = pico_args::Arguments::from_env();

    let args = Args {
        help: args.contains(["-h", "--help"]),
        version: args.contains(["-v", "--version"]),
        commands: args.contains(["-c", "--commands"]),
        cmake: args.contains(["-C", "--cmake"]),
        run_build: args.contains(["-b", "--build"]),
        run_samurai: args.contains(["-s", "--samurai"]),
        file: args.value_from_str(["-f","--file"]).unwrap_or_default(),
    };

    if args.help {
        println!("Usage: ninjify -f FILENAME");
        println!("Converts a JSON or RON build description into a build.ninja file");
        println!();
        println!("Arguments:");
        println!("--file FILENAME  the JSON or RON file to convert");
        println!("--version        show the program version");
        println!("--commands       emit compile_commands.json");
        println!("--build          build with ninja after converting");
        println!("--samurai        build with samurai after converting");
        println!("--cmake          create CMakeLists.txt instead of Ninja build file");
        println!();
        std::process::exit(1);
    }

    if args.version {
        println!("Ninjify version 1.2");
        std::process::exit(1);
    }

    if args.file.is_empty() {
        println!("A build filename must be specified.");
        std::process::exit(1);
    }

    if args.cmake {
        match load_from_file_and_write_cmake(args.file.as_str()) {
            Ok(_) => println!("Generated CMakeLists.txt"),
            Err(string) => println!("{}", string),
        };
    } else {
        match load_from_file_and_write_ninja(args.file.as_str()) {
            Ok(_) => println!("Generated build.ninja"),
            Err(string) => println!("{}", string),
        };

        if args.commands {
            println!("Dumping compile_commands.json");
            write_compile_commands();
        }

        if args.run_build {
            println!("Building with ninja");
            run_build();
        }

        if args.run_samurai {
            println!("Building with samurai");
            run_samurai();
        }

    }

} // main()

#[cfg(test)]
mod test;
