
use super::*;

fn should_be_ninja_string() -> String {
        "
ninja_required_version = 1.6
root = .
builddir = .target

compiler = gcc

linker = gcc

cflags = -Wall -Werror -pthread

nifty_cflags = -fPIE -O3

nifty_ldflags = -O3

nifty_debug_cflags = -fPIE -O0 -g

nifty_debug_ldflags = -O0 -g

rule compile_nifty
  command = $compiler -MMD -MT $out -MF $out.d $cflags $nifty_cflags -c $in -o $out
  description = COMPILE $out
  depfile = $out.d
  deps = gcc

rule link_nifty
  command = $compiler $ldflags $nifty_ldflags -o $out $in $libs
  description = LINK $out

rule compile_nifty_debug
  command = $compiler -MMD -MT $out -MF $out.d $cflags $nifty_debug_cflags -c $in -o $out
  description = COMPILE $out
  depfile = $out.d
  deps = gcc

rule link_nifty_debug
  command = $compiler $ldflags $nifty_debug_ldflags -o $out $in $libs
  description = LINK $out

build $builddir/nifty_main.c.o: compile_nifty $root/main.c

build $builddir/nifty_greeter.c.o: compile_nifty $root/greeter.c

build $builddir/nifty_debug_main.c.o: compile_nifty_debug $root/main.c

build $builddir/nifty_debug_greeter.c.o: compile_nifty_debug $root/greeter.c

build nifty: link_nifty $builddir/nifty_main.c.o $builddir/nifty_greeter.c.o |
    libs = -lpthread -lrt

build nifty_debug: link_nifty_debug $builddir/nifty_debug_main.c.o $builddir/nifty_debug_greeter.c.o |
    libs = -lpthread -lrt

build all: phony nifty nifty_debug\n".to_string()
}

fn should_be_cmake_string() -> String {
        "
cmake_minimum_required(VERSION 3.0)

set(CMAKE_EXPORT_COMPILE_COMMANDS On)

add_compile_options(
    -Wall
    -Werror
    -pthread
    )

target_compile_options(nifty PRIVATE
    -fPIE
    -O3
    )

add_executable(nifty
    main.c
    greeter.c
    )

target_link_libraries(nifty
    -O3
    )

target_compile_options(nifty_debug PRIVATE
    -fPIE
    -O0
    -g
    )

add_executable(nifty_debug
    main.c
    greeter.c
    )

target_link_libraries(nifty_debug
    -O0
    -g
    )
".to_string()
}

#[test]
pub fn test_conversion_from_json() {
    let binfo: Buildinfo = load_from_file("MyFunProject.json").unwrap();
    assert_eq!(binfo.to_ninja_string(), should_be_ninja_string());
}

#[test]
pub fn test_conversion_from_ron() {
    let binfo: Buildinfo = load_from_file("MyFunProject.ron").unwrap();
    assert_eq!(binfo.to_ninja_string(), should_be_ninja_string());
}

#[test]
pub fn test_cmake_from_json() {
    let binfo: Buildinfo = load_from_file("MyFunProject.json").unwrap();
    assert_eq!(binfo.to_cmake_string(), should_be_cmake_string());
}

#[test]
pub fn test_cmake_from_ron() {
    let binfo: Buildinfo = load_from_file("MyFunProject.ron").unwrap();
    assert_eq!(binfo.to_cmake_string(), should_be_cmake_string());
}
